from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenetv1_ssd import create_mobilenetv1_ssd, create_mobilenetv1_ssd_predictor
from vision.utils.misc import Timer
import cv2
import sys
import numpy as np
import urllib
import urllib.request
import csv
import ast

def boxes_for_resize(boxes, size, padding, new_size): #boxes from (x,y,h,w) to (x1,y1,x2,y2) ()
    width, height = size
    new_width, new_height = new_size
    top, bottom, left, right = padding

    boxes[:, 0] = (boxes[:, 0] + left) * new_width/width
    boxes[:, 1] = (boxes[:, 1] + bottom) * new_height/height
    temp_height = np.copy(boxes[:, 2])
    boxes[:, 2] = boxes[:, 0] + (new_width/width)*boxes[:, 3]
    boxes[:, 3] = boxes[:, 1] + (new_height/height)*temp_height
    return np.float32(boxes)

if len(sys.argv) < 5:
    print('Usage: python ssd_test_cone_image.py <model path> <label path> <image aws url> <csv file of ground truth boxes>')
    sys.exit(0)
model_path = sys.argv[1]
label_path = sys.argv[2]
image_url = sys.argv[3]
csv_boxes = sys.argv[4]

net_type = "mobilenet-v1-ssd"

class_names = [name.strip() for name in open(label_path).readlines()]
num_classes = len(class_names)
if net_type == "mobilenet-v1-ssd":
    net = create_mobilenetv1_ssd(num_classes, is_test=True)
else:
    net = create_vgg_ssd(num_classes, is_test=True)
net.load(model_path)
net.cpu()
if net_type == "mobilenet-v1-ssd":
    predictor = create_mobilenetv1_ssd_predictor(net, candidate_size=200)
else:
    predictor = create_vgg_ssd_predictor(net, candidate_size=200)

#get image
resp = urllib.request.urlopen(image_url)
orig_image = np.asarray(bytearray(resp.read()), dtype="uint8")
orig_image = cv2.imdecode(orig_image, cv2.IMREAD_COLOR)

#get ground truth boxes 
given_boxes = []
with open(csv_boxes) as f:
    next(f) #skip first line
    csv_reader = csv.reader(f, delimiter='[')
    for row in csv_reader:
        image_loc = row[0].split(',')[1]
        if image_loc == image_url:
            for i in range(1,len(row)):
                box_string = row[i]
                given_boxes.append(ast.literal_eval("[" + row[i][:box_string.find(']')] + "]")) 
            break; 
given_boxes = np.array(given_boxes, dtype=np.float32)

#pad images before resize to maintain aspect ratio
h, w, _ = orig_image.shape
dim_diff = np.abs(h - w)
pad1 = dim_diff // 2 #upper/left padding
pad2 = dim_diff - (dim_diff // 2) #lower/right padding
#determine if padding x or y axis 
top, bottom, left, right = 0, 0, 0, 0
if h<=w:
    top = pad1
    bottom = pad2
else:
    left = pad1
    right = pad2

orig_image = cv2.copyMakeBorder(orig_image, top, bottom, left, right, cv2.BORDER_CONSTANT, value=[128,128,128]) #gray border

#resize to 416x416 and get boxes in terms of size ratios
boxes = boxes_for_resize(given_boxes, tuple(orig_image.shape[1::-1]), (top,bottom,left,right), (416,416))
orig_image = cv2.resize(orig_image, (416,416))

image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
print("image size", np.shape(image))
boxes, labels, probs = predictor.predict(image, 10, 0.4)

#visualize predicted boxes
for i in range(boxes.size(0)):
    box = boxes[i, :]
    cv2.rectangle(orig_image, (box[0], box[1]), (box[2], box[3]), (0, 0, 255), 1)
    print("predicted box", box)
    #label = f"""{voc_dataset.class_names[labels[i]]}: {probs[i]:.2f}"""
    label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
    # cv2.putText(orig_image, label,
    #             (box[0] + 20, box[1] + 40),
    #             cv2.FONT_HERSHEY_SIMPLEX,
    #             0.25,  # font scale
    #             (255, 0, 255),
    #             2)  # line type

print("--------")

#visualize ground truth boxes 
for box in given_boxes:
    x1, y1, x2, y2 = box
    cv2.rectangle(orig_image, (x1,y1), (x2,y2), (255,0,0), 1)
    print("ground truth box", box)

path = "run_ssd_example_output.jpg"
cv2.imwrite(path, orig_image)
print(f"Found {len(probs)} objects. The output image is {path}")
